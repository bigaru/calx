/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./node_modules/@vechaiui/**/*.{js,ts,jsx,tsx}', './src/**/*.{html,js}', './public/index.html'],
  theme: {
    extend: {},
  },
  plugins: [require('@tailwindcss/forms'), require('@vechaiui/core')],
};
