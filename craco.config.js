const { ESLINT_MODES } = require('@craco/craco');

module.exports = {
  reactScriptsVersion: 'react-scripts',
  style: {
    postcss: {
      plugins: {
        tailwindcss: {},
        autoprefixer: {},
      },
    },
  },
  eslint: {
    mode: ESLINT_MODES.file,
  },
  babel: {},
  typescript: {
    enableTypeChecking: true,
  },
  webpack: {},
  jest: {},
  devServer: {},
  plugins: [],
};
